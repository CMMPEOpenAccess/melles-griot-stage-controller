﻿// ReSharper disable once CheckNamespace
// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public class LimitState2
    {
        public LimitState X { get; }
        
        public LimitState Y { get; }

        public LimitState2(LimitState x, LimitState y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
