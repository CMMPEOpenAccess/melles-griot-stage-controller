﻿using System;

// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public class IntPoint2
    {
        public int X { get; }
        
        public int Y { get; }

        public IntPoint2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int Max()
        {
            return Math.Max(X, Y);
        }

        public int Min()
        {
            return Math.Min(X, Y);
        }

        public IntPoint2 Abs()
        {
            return new IntPoint2(Math.Abs(X), Math.Abs(Y));
        }

        public static IntPoint2 operator+ (IntPoint2 l, IntPoint2 r)
        {
            return new IntPoint2(l.X + r.X, l.Y + r.Y);
        }

        public static IntPoint2 operator- (IntPoint2 l, IntPoint2 r)
        {
            return new IntPoint2(l.X - r.X, l.Y - r.Y);
        }

        public static IntPoint2 operator/ (IntPoint2 l, IntPoint2 r)
        {
            return new IntPoint2(l.X / r.X, l.Y / r.Y);
        }

        public static IntPoint2 operator* (IntPoint2 l, IntPoint2 r)
        {
            return new IntPoint2(l.X * r.X, l.Y * r.Y);
        }

        public static IntPoint2 operator* (IntPoint2 l, int r)
        {
            return new IntPoint2(l.X * r, l.Y * r);
        }

        public static IntPoint2 operator/ (IntPoint2 l, int r)
        {
            return new IntPoint2(l.X / r, l.Y / r);
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
