﻿using System;
using canlibCLSNET;

// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public sealed class Stage3Axis : IDisposable
    {
        private readonly Stage1Axis _xAxis;
        private readonly Stage1Axis _yAxis;
        private readonly Stage1Axis _zAxis;
        
        public bool ErrorState => _xAxis.ErrorState || _yAxis.ErrorState || _zAxis.ErrorState;
        
        public Stage3Axis() : this(34, 38, 32)
        {
        }

        public Stage3Axis(int xAxisChannel, int yAxisChannel, int zAxisChannel)
        {
            _xAxis = new Stage1Axis(xAxisChannel);
            _yAxis = new Stage1Axis(yAxisChannel);
            _zAxis = new Stage1Axis(zAxisChannel);
        }
        
        public void SendMessage(byte[] message, long timeout = 1000)
        {
            _xAxis.SendMessage(message, timeout);
            _yAxis.SendMessage(message, timeout);
            _zAxis.SendMessage(message, timeout);
        }
        
        public bool IsConnected()
        {
            return 
                _xAxis.IsConnected() &&
                _yAxis.IsConnected() &&
                _zAxis.IsConnected();
        }

        public IntPoint3 GetPosition()
        {
            return new IntPoint3(
                _xAxis.GetPosition(),
                _yAxis.GetPosition(),
                _zAxis.GetPosition());
        }

        public void SetPosition(int value)
        {
            _xAxis.SetPosition(value);
            _yAxis.SetPosition(value);
            _zAxis.SetPosition(value);
        }

        public void SetPosition(IntPoint3 value)
        {
            _xAxis.SetPosition(value.X);
            _yAxis.SetPosition(value.Y);
            _zAxis.SetPosition(value.Z);
        }

        public IntPoint3 GetAcceleration()
        {
            return new IntPoint3(
                _xAxis.GetAcceleration(),
                _yAxis.GetAcceleration(),
                _zAxis.GetAcceleration());
        }

        public void SetAcceleration(int value)
        {
            _xAxis.SetAcceleration(value);
            _yAxis.SetAcceleration(value);
            _zAxis.SetAcceleration(value);
        }

        public void SetAcceleration(IntPoint3 value)
        {
            _xAxis.SetAcceleration(value.X);
            _yAxis.SetAcceleration(value.Y);
            _zAxis.SetAcceleration(value.Z);
        }

        public IntPoint3 GetMaxVelocity()
        {
            return new IntPoint3(
                _xAxis.GetMaxVelocity(),
                _yAxis.GetMaxVelocity(),
                _zAxis.GetMaxVelocity());
        }

        public void SetMaxVelocity(int value)
        {
            _xAxis.SetMaxVelocity(value);
            _yAxis.SetMaxVelocity(value);
            _zAxis.SetMaxVelocity(value);
        }

        public void SetMaxVelocity(IntPoint3 value)
        {
            _xAxis.SetMaxVelocity(value.X);
            _yAxis.SetMaxVelocity(value.Y);
            _zAxis.SetMaxVelocity(value.Z);
        }

        public IntPoint3 GetInitVelocity()
        {
            return new IntPoint3(
                _xAxis.GetInitVelocity(),
                _yAxis.GetInitVelocity(),
                _zAxis.GetInitVelocity());
        }

        public void SetInitVelocity(int value)
        {
            _xAxis.SetInitVelocity(value);
            _yAxis.SetInitVelocity(value);
            _zAxis.SetInitVelocity(value);
        }

        public void SetInitVelocity(IntPoint3 value)
        {
            _xAxis.SetInitVelocity(value.X);
            _yAxis.SetInitVelocity(value.Y);
            _zAxis.SetInitVelocity(value.Z);
        }

        public IntPoint3 GetCurrentVelocity()
        {
            return new IntPoint3(
                _xAxis.GetCurrentVelocity(),
                _yAxis.GetCurrentVelocity(),
                _zAxis.GetCurrentVelocity());
        }

        internal void SetGearReduction(GearReduction value)
        {
            _xAxis.SetGearReduction(value);
            _yAxis.SetGearReduction(value);
            _zAxis.SetGearReduction(value);
        }
        
        public LimitState3 GetLimitSwitchStatus()
        {
            return new LimitState3(
                _xAxis.GetLimitSwitchStatus(),
                _yAxis.GetLimitSwitchStatus(),
                _zAxis.GetLimitSwitchStatus());
        }

        public void MoveAbsolute(IntPoint3 value)
        {
            Move(value, false);
        }

        public void MoveRelative(IntPoint3 value)
        {
            Move(value, true);
        }

        public void Move(IntPoint3 value, bool relative)
        {
            _xAxis.Move(value.X, relative);
            _yAxis.Move(value.Y, relative);
            _zAxis.Move(value.Z, relative);
        }

        public void MoveToMax()
        {
            _xAxis.MoveToMax();
            _yAxis.MoveToMax();
            _zAxis.MoveToMax();
        }

        public void MoveToMin()
        {
            _xAxis.MoveToMin();
            _yAxis.MoveToMin();
            _zAxis.MoveToMin();
        }
        
        public void MoveToEnd(bool max)
        {
            _xAxis.MoveToEnd(max);
            _yAxis.MoveToEnd(max);
            _zAxis.MoveToEnd(max);
        }
        
        public void Stop()
        {
            _xAxis.Stop();
            _yAxis.Stop();
            _zAxis.Stop();
        }
        
        public void HardStop()
        {
            _xAxis.HardStop();
            _yAxis.HardStop();
            _zAxis.HardStop();
        }

        public void SetCurrentLocationAsDatum()
        {
            _xAxis.SetCurrentLocationAsDatum();
            _yAxis.SetCurrentLocationAsDatum();
            _zAxis.SetCurrentLocationAsDatum();
        }

        ~Stage3Axis()
        {
            Dispose(false);
        }

        internal void CheckStatus(Canlib.canStatus status, string method)
        {
            _xAxis.CheckStatus(status, method);
            _yAxis.CheckStatus(status, method);
            _zAxis.CheckStatus(status, method);
        }

        private void ReleaseUnmanagedResources()
        {
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _xAxis?.Dispose();
                _yAxis?.Dispose();
                _zAxis?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
