﻿using System;
using canlibCLSNET;
using MellesGriotStageController;

// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public sealed class Stage2Axis : IDisposable
    {
        private readonly Stage1Axis _xAxis;
        private readonly Stage1Axis _yAxis;
        
        public bool ErrorState => _xAxis.ErrorState || _yAxis.ErrorState;
        
        public Stage2Axis() : this(34, 38, 32)
        {
        }

        public Stage2Axis(int xAxisChannel, int yAxisChannel, int zAxisChannel)
        {
            _xAxis = new Stage1Axis(xAxisChannel);
            _yAxis = new Stage1Axis(yAxisChannel);
        }
        
        public void SendMessage(byte[] message, long timeout = 1000)
        {
            _xAxis.SendMessage(message, timeout);
            _yAxis.SendMessage(message, timeout);
        }
        
        public bool IsConnected()
        {
            return 
                _xAxis.IsConnected() &&
                _yAxis.IsConnected();
        }

        public IntPoint2 GetPosition()
        {
            return new IntPoint2(
                _xAxis.GetPosition(),
                _yAxis.GetPosition());
        }

        public void SetPosition(int value)
        {
            _xAxis.SetPosition(value);
            _yAxis.SetPosition(value);
        }

        public void SetPosition(IntPoint2 value)
        {
            _xAxis.SetPosition(value.X);
            _yAxis.SetPosition(value.Y);
        }

        public IntPoint2 GetAcceleration()
        {
            return new IntPoint2(
                _xAxis.GetAcceleration(),
                _yAxis.GetAcceleration());
        }

        public void SetAcceleration(int value)
        {
            _xAxis.SetAcceleration(value);
            _yAxis.SetAcceleration(value);
        }

        public void SetAcceleration(IntPoint2 value)
        {
            _xAxis.SetAcceleration(value.X);
            _yAxis.SetAcceleration(value.Y);
        }

        public IntPoint2 GetMaxVelocity()
        {
            return new IntPoint2(
                _xAxis.GetMaxVelocity(),
                _yAxis.GetMaxVelocity());
        }

        public void SetMaxVelocity(int value)
        {
            _xAxis.SetMaxVelocity(value);
            _yAxis.SetMaxVelocity(value);
        }

        public void SetMaxVelocity(IntPoint2 value)
        {
            _xAxis.SetMaxVelocity(value.X);
            _yAxis.SetMaxVelocity(value.Y);
        }

        public IntPoint2 GetInitVelocity()
        {
            return new IntPoint2(
                _xAxis.GetInitVelocity(),
                _yAxis.GetInitVelocity());
        }

        public void SetInitVelocity(int value)
        {
            _xAxis.SetInitVelocity(value);
            _yAxis.SetInitVelocity(value);
        }

        public void SetInitVelocity(IntPoint2 value)
        {
            _xAxis.SetInitVelocity(value.X);
            _yAxis.SetInitVelocity(value.Y);
        }

        public IntPoint2 GetCurrentVelocity()
        {
            return new IntPoint2(
                _xAxis.GetCurrentVelocity(),
                _yAxis.GetCurrentVelocity());
        }

        internal void SetGearReduction(GearReduction value)
        {
            _xAxis.SetGearReduction(value);
            _yAxis.SetGearReduction(value);
        }
        
        public LimitState2 GetLimitSwitchStatus()
        {
            return new LimitState2(
                _xAxis.GetLimitSwitchStatus(),
                _yAxis.GetLimitSwitchStatus());
        }

        public void MoveAbsolute(IntPoint2 value)
        {
            Move(value, false);
        }

        public void MoveRelative(IntPoint2 value)
        {
            Move(value, true);
        }

        public void Move(IntPoint2 value, bool relative)
        {
            _xAxis.Move(value.X, relative);
            _yAxis.Move(value.Y, relative);
        }

        public void MoveToMax()
        {
            _xAxis.MoveToMax();
            _yAxis.MoveToMax();
        }

        public void MoveToMin()
        {
            _xAxis.MoveToMin();
            _yAxis.MoveToMin();
        }
        
        public void MoveToEnd(bool max)
        {
            _xAxis.MoveToEnd(max);
            _yAxis.MoveToEnd(max);
        }
        
        public void Stop()
        {
            _xAxis.Stop();
            _yAxis.Stop();
        }
        
        public void HardStop()
        {
            _xAxis.HardStop();
            _yAxis.HardStop();
        }

        public void SetCurrentLocationAsDatum()
        {
            _xAxis.SetCurrentLocationAsDatum();
            _yAxis.SetCurrentLocationAsDatum();
        }

        ~Stage2Axis()
        {
            Dispose(false);
        }

        internal void CheckStatus(Canlib.canStatus status, string method)
        {
            _xAxis.CheckStatus(status, method);
            _yAxis.CheckStatus(status, method);
        }

        private void ReleaseUnmanagedResources()
        {
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
                _xAxis?.Dispose();
                _yAxis?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
