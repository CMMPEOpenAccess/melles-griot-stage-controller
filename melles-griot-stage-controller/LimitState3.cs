﻿// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public class LimitState3
    {
        public LimitState X { get; }
        
        public LimitState Y { get; }
        
        public LimitState Z { get; }

        public LimitState3(LimitState x, LimitState y, LimitState z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }
    }
}