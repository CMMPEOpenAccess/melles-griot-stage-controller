﻿// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public enum GearReduction
    {
        Double,
        Standard,
        Half,
        Quarter
    };
}
