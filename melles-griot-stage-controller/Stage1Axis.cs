﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using canlibCLSNET;

// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public sealed class Stage1Axis : IDisposable
    {
        private const int TargetBitRate = Canlib.canBITRATE_125K;
        private readonly int _handle;
        private readonly int _remoteID;
        private Canlib.canStatus _status;
        public bool ErrorState { get; internal set; }
        private readonly Stopwatch _stopwatch = new Stopwatch();
        
        private const int ChannelID = 0; 
        private const int MasterID = 64; 
        private const double VelocityToMetricFactor = 0.0000943; // Measured by George S. D. Gordon with a micrometer
        private const double PositionToMetricFactor = 0.0000236; // Measured by George S. D. Gordon with a micrometer
        private const int MaxVelocity = 140000; 

        public static int[] ScanForStages(int start, int end, int bitRate = TargetBitRate, int timeout = 1000)
        {
            Debug.Assert(start >= 0);
            Debug.Assert(end >= 0);
            Debug.Assert(start <= end);

            Console.WriteLine("Scanning for 1-Axis Stages");

            var results = new List<int>();
            
            Canlib.canInitializeLibrary();
            int handle = Canlib.canOpenChannel(0, Canlib.canOPEN_ACCEPT_VIRTUAL);
            
            var status = Canlib.canSetBusParams(handle, bitRate, 0, 0, 0, 0, 0);
            if (status != Canlib.canStatus.canOK) return null;

            status = Canlib.canBusOn(handle);
            if (status != Canlib.canStatus.canOK) return null;

            // Strategy is ask every id for its position and catalogue any useful responses
            for (int i = start; i <= end; i++)
            {
                // Ask for position
                status = Canlib.canWrite(handle, i, PadBytes((byte) '?', (byte) 'P'), 8, 0);
                if (status != Canlib.canStatus.canOK) return null;

                // Wait for writing to finish
                status = Canlib.canWriteSync(handle, timeout);
                if (status != Canlib.canStatus.canOK) return null;

                // Read result
                var data = new byte[8];
                status = Canlib.canReadWait(handle, out var id, data, out _, out _, out _, timeout);

                // See if we get a result from the master
                while(status == Canlib.canStatus.canOK)
                {
                    if(id == MasterID)
                        results.Add(i);

                    status = Canlib.canRead(handle, out _, data, out _, out _, out _);
                }
            }
            
            
            // Takes the channel off bus
            Console.WriteLine("Going off bus");
            Canlib.canBusOff(handle);

            // Closes the channel
            Console.WriteLine("Closing channel 0");
            Canlib.canClose(handle);

            // Return the ids that responded
            return results.ToArray();
        }

        public Stage1Axis(int remoteID)
        {
            Console.WriteLine("Initializing 1-Axis Stage");
            _remoteID = remoteID;

            Canlib.canInitializeLibrary();
            
            // Open channel. Will be 0 in simple systems. 
            _handle = Canlib.canOpenChannel(ChannelID, Canlib.canOPEN_ACCEPT_VIRTUAL);
            CheckStatus((Canlib.canStatus)_handle, "canSetBusParams");
            if (_status != Canlib.canStatus.canOK) return;

            // Turn off channel echo
            Canlib.canIoCtl(_handle, Canlib.canIOCTL_SET_LOCAL_TXECHO, 0);
            
            // Dump channel details to console
            Console.Write(GetChannelDetails());
            
            // Set bitrate and bus parameters
            _status = Canlib.canSetBusParams(_handle, TargetBitRate, 0, 0, 0, 0, 0);
            CheckStatus(_status, "canSetBusParams");
            if (_status != Canlib.canStatus.canOK) return;
            
            // Turn on the can bus
            _status = Canlib.canBusOn(_handle);
            CheckStatus(_status, "canBusOn");
            if (_status != Canlib.canStatus.canOK) return;

            // Try and restore some settings that wont destroy the attached stage
            RestoreDefaultSettings();
        }

        public bool IsConnected()
        {
            return _status == Canlib.canStatus.canOK;
        }

        public void RestoreDefaultSettings()
        {
            //SetCurrentLocationAsDatum();
            
            SetGearReduction(GearReduction.Standard);
            SetAcceleration(MetricToVelocity(2));
            SetMaxVelocity(MetricToVelocity(2));
            
            SendMessage(PadBytes((byte) '>', 0, 2));
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) '<', 0, 4));
        }

        public double VelocityToMetric(int value)
        {
            return VelocityToMetricFactor * value;
        }

        public double PositionToMetric(int value)
        {
            return PositionToMetricFactor * value;
        }

        public int MetricToVelocity(double value)
        {
            return (int) (value / VelocityToMetricFactor);
        }

        public int MetricToPosition(double value)
        {
            return (int) (value / PositionToMetricFactor);
        }

        public void SendMessage(byte[] message, long timeout = 1000)
        {
            Canlib.canFlushReceiveQueue(_handle);

            _status = Canlib.canWrite(_handle, _remoteID, message, 8, 0);
            CheckStatus(_status, "canWrite");

            _status = Canlib.canWriteSync(_handle, timeout);
            CheckStatus(_status, "canWriteSync");
        }

        public byte[] ReadMessage(int desiredID, long timeout = 1000)
        {
            _stopwatch.Stop();
            _stopwatch.Reset();
            var data = new byte[8];
            var timeSoFar = _stopwatch.ElapsedMilliseconds;
            while(timeSoFar < timeout)
            {
                _stopwatch.Start();
                _status = Canlib.canReadWait(_handle, out var id, data, out _, out var flags, out _, timeout);

                while(_status == Canlib.canStatus.canOK)
                {
                    if ((flags & Canlib.canMSG_ERROR_FRAME) > 0)
                        ErrorState = true;

                    if(id == desiredID)
                        return data;

                    _status = Canlib.canRead(_handle, out id, data, out _, out _, out _);
                }

                if(_status != Canlib.canStatus.canERR_NOMSG)
                {
                    CheckStatus(_status, "canRead/canReadWait");
                    return null;
                }

                _stopwatch.Stop();
                timeSoFar = _stopwatch.ElapsedMilliseconds;
            }

            ErrorState = true;
            return null;
        }

        private byte[] Int32ToBytes(int value)
        {
            try
            {
                byte[] bytes = BitConverter.GetBytes(value);
                // Our stage is little endian
                if(!BitConverter.IsLittleEndian)
                    Array.Reverse(bytes);
                return bytes;
            }
            catch(Exception e)
            {
                Console.WriteLine($"Int32 to byte[] conversion error. Message: {e.Message}");
                ErrorState = true;
                return null;
            }
        }

        private int BytesToInt32(byte[] data)
        {
            Debug.Assert(data.Length == 4);

            try
            {
                // Our stage is little endian
                if(!BitConverter.IsLittleEndian)
                    Array.Reverse(data);
                
                return BitConverter.ToInt32(data, 0);
            }
            catch(Exception e)
            {
                Console.WriteLine($"Byte[] to int conversion error. Message: {e.Message}");
                ErrorState = true;
                return 0;
            }
        }

        private int GearReductionToInt(GearReduction gearReduction)
        {
            switch(gearReduction)
            {
                case GearReduction.Double:
                    return 8;
                case GearReduction.Standard:
                    return 4;
                case GearReduction.Half:
                    return 2;
                case GearReduction.Quarter:
                    return 1;
                default:
                    return 4;
            }
        }

        public int GetPosition()
        {
            return GenericGetIntQuery('P');
        }

        public void SetPosition(int value)
        {
            GenericSetIntQuery(value, 'P');
        }

        public int GetAcceleration()
        {
            return GenericGetIntQuery('S');
        }

        public void SetAcceleration(int value)
        {
            GenericSetIntQuery(value, 'S', MaxVelocity);
        }

        public int GetMaxVelocity()
        {
            return GenericGetIntQuery('R');
        }

        public void SetMaxVelocity(int value)
        {
            GenericSetIntQuery(value, 'R', MaxVelocity);
        }

        public int GetInitVelocity()
        {
            return GenericGetIntQuery('F');
        }

        public void SetInitVelocity(int value)
        {
            GenericSetIntQuery(value, 'F', MaxVelocity);
        }

        public int GetCurrentVelocity()
        {
            return GenericGetIntQuery('V');
        }

        internal void GenericSetIntQuery(int value, char token, int maxValue = int.MaxValue, int minValue = int.MinValue)
        {
            SendMessage(PadBytes((byte) token,  Int32ToBytes(value)));
        }

        internal int GenericGetIntQuery(char token)
        {
            SendMessage(PadBytes((byte) '?', (byte) token));
            byte[] data = ReadMessage(MasterID);
            if (!(data?.Length > 0))
            {
                ErrorState = true;
                return 0;
            }

            //Console.WriteLine($"First Number: {data[0]} Integer: {BytesToInt32(data.Skip(1).Take(4).ToArray())}");

            return BytesToInt32(data.Skip(1).Take(4).ToArray());
        }
        
        public void SetGearReduction(GearReduction value)
        {
            SendMessage(PadBytes((byte) 'M',  Int32ToBytes(GearReductionToInt(value))));
        }
        
        public LimitState GetLimitSwitchStatus()
        {
            SendMessage(PadBytes((byte) '?', (byte) 'B'));
            byte[] data = ReadMessage(MasterID);
            if (!(data?.Length > 0))
            {
                return null;
            }

            var bits = new BitArray(data.Skip(1).Take(1).ToArray());
            return new LimitState(!bits[6], !bits[5]);
        }

        public void MoveAbsolute(int value)
        {
            Move(value, false);
        }

        public void MoveRelative(int value)
        {
            Move(value, true);
        }

        public void Move(int value, bool relative)
        {
            var currentPosition = GetPosition();
            var maxVelocity = GetMaxVelocity();
            if (ErrorState) return;

            var newPosition = relative ? currentPosition + value : value;
            var timeout = Math.Abs(currentPosition - newPosition) * 3000 / maxVelocity;

            SetPosition(newPosition);
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) 'O'));
            if (ErrorState) return;

            if (!(ReadMessage(MasterID, timeout)?.Length > 0))
            {
                ErrorState = true;
            }
        }

        public void MoveToMax()
        {
            MoveToEnd(true);
        }

        public void MoveToMin()
        {
            MoveToEnd(false);
        }
        
        public void MoveToEnd(bool max)
        {
            SendMessage(PadBytes((byte) (max ? '+' : '-')));
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) 'C', 1));
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) 'G', 0));
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) 'O'));
            if (ErrorState) return;
            
            SendMessage(PadBytes((byte) 'C'));
        }
        
        public void Stop()
        {
            SendMessage(PadBytes((byte) 'K', 0));

            if (!(ReadMessage(MasterID)?.Length > 0))
            {
                ErrorState = true;
            }
        }
        
        public void HardStop()
        {
            SendMessage(PadBytes((byte) 'K', 1));

            if (!(ReadMessage(MasterID)?.Length > 0))
            {
                ErrorState = true;
            }
        }

        public void SetCurrentLocationAsDatum()
        {
            SendMessage(PadBytes((byte) 'A'));
        }

        public static byte[] PadBytes(params byte[] bytes)
        {
            // TODO: Handle messages over 8 bytes long
            byte[] data = {0, 0, 0, 0, 0, 0, 0, 0};
            int i = 0;
            foreach (var b in bytes)
            {
                data[i] = b;
                i++;
            }

            return data;
        }

        public static byte[] PadBytes(byte b1, byte[] bytes)
        {
            // TODO: Handle messages over 8 bytes long
            byte[] data = {0, 0, 0, 0, 0, 0, 0, 0};
            data[0] = b1;
            int i = 1;
            foreach (var b in bytes)
            {
                data[i] = b;
                i++;
            }

            return data;
        }

        ~Stage1Axis()
        {
            Dispose(false);
        }

        internal void CheckStatus(Canlib.canStatus status, string method)
        {
            if (status < 0)
            {
                Canlib.canGetErrorText(status, out var errorText);
                Console.WriteLine(method + " failed: " + errorText);
                ErrorState = true;
            }
        }

        private void ReleaseUnmanagedResources()
        {
            // Takes the channel off bus
            Console.WriteLine("Going off bus");
            _status = Canlib.canBusOff(_handle);
            CheckStatus(_status, "canBusOff");

            // Closes the channel
            Console.WriteLine("Closing channel 0");
            _status = Canlib.canClose(_handle);
            CheckStatus(_status, "canClose");
        }

        private void Dispose(bool disposing)
        {
            ReleaseUnmanagedResources();
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        public string GetChannelDetails()
        {
            StringBuilder builder = new StringBuilder();
            
            builder.AppendLine($"Dumping information about channel: \"{ChannelID}\"");
            
            // ReSharper disable StringLiteralTypo
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CHANNEL_NAME, out var value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CHANNEL_NAME: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_NUMBER, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_NUMBER: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_SERIAL_NO, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_SERIAL_NO: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_HARDWARE_REV, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_HARDWARE_REV: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_SERIAL_NO, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_SERIAL_NO: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DRIVER_NAME, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DRIVER_NAME: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_FIRMWARE_REV, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_FIRMWARE_REV: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_CARD_TYPE, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_CARD_TYPE: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_TRANS_TYPE, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_TRANS_TYPE: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_TRANS_SERIAL_NO, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_TRANS_SERIAL_NO: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_TRANS_CAP, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_TRANS_CAP: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_TRANS_UPC_NO, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_TRANS_UPC_NO: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DLL_FILETYPE, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DLL_FILETYPE: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DLL_FILE_VERSION, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DLL_FILE_VERSION: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DLL_PRODUCT_VERSION, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DLL_PRODUCT_VERSION: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DEVICE_PHYSICAL_POSITION, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DEVICE_PHYSICAL_POSITION: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_UI_NUMBER, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_UI_NUMBER: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_TIMESYNC_ENABLED, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_TIMESYNC_ENABLED: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_ROUNDTRIP_TIME, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_ROUNDTRIP_TIME: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DRIVER_NAME, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DRIVER_NAME: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DRIVER_FILE_VERSION, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DRIVER_FILE_VERSION: \"{value}\"");
            _status = Canlib.canGetChannelData(ChannelID, Canlib.canCHANNELDATA_DRIVER_PRODUCT_VERSION, out value);
            if (value != null) builder.AppendLine($"canCHANNELDATA_DRIVER_PRODUCT_VERSION: \"{value}\"");
            // ReSharper restore StringLiteralTypo

            return builder.ToString();
        }
    }
}
