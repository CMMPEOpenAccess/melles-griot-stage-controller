﻿using System;

// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{
    public class IntPoint3
    {
        public int X { get; }
        
        public int Y { get; }
        
        public int Z { get; }

        public IntPoint3(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public int Max()
        {
            return Math.Max(X, Math.Max(Y, Z));
        }

        public int Min()
        {
            return Math.Min(X, Math.Min(Y, Z));
        }

        public IntPoint3 Abs()
        {
            return new IntPoint3(Math.Abs(X), Math.Abs(Y), Math.Abs(Z));
        }

        public static IntPoint3 operator+ (IntPoint3 l, IntPoint3 r)
        {
            return new IntPoint3(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
        }

        public static IntPoint3 operator- (IntPoint3 l, IntPoint3 r)
        {
            return new IntPoint3(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
        }

        public static IntPoint3 operator/ (IntPoint3 l, IntPoint3 r)
        {
            return new IntPoint3(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
        }

        public static IntPoint3 operator* (IntPoint3 l, IntPoint3 r)
        {
            return new IntPoint3(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
        }

        public static IntPoint3 operator* (IntPoint3 l, int r)
        {
            return new IntPoint3(l.X * r, l.Y * r, l.Z * r);
        }

        public static IntPoint3 operator/ (IntPoint3 l, int r)
        {
            return new IntPoint3(l.X / r, l.Y / r, l.Z / r);
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }
    }
}
