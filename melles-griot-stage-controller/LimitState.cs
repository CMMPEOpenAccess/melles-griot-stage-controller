﻿// ReSharper disable once CheckNamespace
namespace MellesGriotStageController
{    public sealed class LimitState
    {
        public bool MinSwitchHit { get; }
        
        public bool MaxSwitchHit { get; }

        public LimitState(bool minSwitchHit, bool maxSwitchHit)
        {
            MinSwitchHit = minSwitchHit;
            MaxSwitchHit = maxSwitchHit;
        }

        public override string ToString()
        {
            return $"[{MinSwitchHit}, {MaxSwitchHit}]";
        }
    }
}

