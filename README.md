# README

_**Warning! This was developed as a tool to aid my (Peter J. Christopher) PhD. It is comparitively stable for what I used it for but is definitely not feature complete. Use this at your own risk!**_

## Introduction

This package was designed to interface to a Melles Griot NanoMax TS 3-axis stage with piezoelectric actuators. Each axis is driven using a Melles Griot 17MST001 and a Melles Griot 17MPZ001 mounted in a Melles Griot 17MMR001 unit. Thorlabs still sell [the stages](https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=2386) but they have changed the controllers. George S. D. Gordon managed to reverse engineer the old CAN bus command syntax and I've wrapped it here in a C# utility. This is free to use software and feel free to [mail me](mailto:pjc209@cam.ac.uk) any questions.

## Acknowledgements

George S. D. Gordon is responsible for reverse engineering the commands for the 17MST001 and designing the original electronic circuitry.

## Getting Started

In order to connect to the PC you'll need a CAN bus setup. We use a Kvaser Leaf Light HS v2 to connect to a RS-232/9 Pin D-sub cable wired as shown [here](images/canbus.jpg?raw=true).

Our arrangement looks like [this](images/canbus.jpg?raw=true)

The software side of things is straightforward. The hardest thing is finding the IDs associated with each controller. We used a brute force search starting from zero for ours.

## Notes

* The Kvaser Leaf Light HS v2 we use has its own canlibCLSNET api. You will have to swap that out if you want to use a different CAN bus controller.

## License

Copyright 2019 Peter J. Christopher

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk